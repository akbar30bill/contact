#include <iostream>
#include <vector>
#include <string>

#define PHONE_NUMBER_SIZE 11
#define COMMAND_PLACE 0
#define FIRST_COMMAND_PLACE 1
#define ID_PLACE 1
#define TXT_PLACE 1
#define PASS 1
#define FAIL 0

using namespace std;

class contact {
public:

	int id;
	string name;
	string last_name;
	string email;
	string phone_number;
	string address;



	void set_name(string input_name)
	{
		name = input_name;
		return;
	}
	void set_last_name(string input_last_name)
	{
		last_name = input_last_name;
		return;
	}
	bool set_email (string input_email)
	{
		for ( int i = 0 ; i < input_email.size() ; i++ )
		{
			if ( input_email[i] == '@')
			{
				if ( input_email[i+1] == '.')
				{
					return 0;
				}
			}
		}
		email = input_email;
		return 1;
	}
	bool set_phone_number ( string input_phone_number )
	{
		if (!((input_phone_number[0] == 0) && (input_phone_number[1] == 9) && (input_phone_number.size() == PHONE_NUMBER_SIZE ) ))
		{
			return 0;
		}
		phone_number = input_phone_number;
		return 1;
	}
	void set_address ( string input_address )
	{
		address = input_address;
		return;
	}
	bool search_contact ( string input )
	{
		if ((name.find(input) == string::npos) && (last_name.find(input) == string::npos) && (email.find(input) == string::npos) && (phone_number.find(input) == string::npos) && (address.find(input) == string::npos))
		{
			return 1;
		}
		return 0;
	}
	void set_id ( int input_id ){
		id = input_id;
		return;
	}
};	

vector<string> get_command ( string& command ){
	vector<string> command_vector;
	string temp = "";
	for ( int i = 0 ; i < command.size() ; i++ )
	{
		if (command[i] == ' ')
		{
			command_vector.push_back(temp);
			temp = "";
		}
		else
		{
			temp.push_back(command[i]);
		}
	command_vector.push_back(temp);
	}
	return command_vector;
}
bool test_get_command (){
	string str = "hello my name is ali";
	vector<string> v = get_command(str);
	if ( v[2] == "name" )
	{
		return PASS;
	}
	else
	{
		return FAIL;
	}
}

bool add (int id , vector<string>& command_vector , vector<contact>& contact_vector){
	contact new_contact;
	for ( int command_place = FIRST_COMMAND_PLACE ; command_place < command_vector.size() ; command_place++ )
	{
		if ( command_vector[command_place] == "-f" )
		{
			string temp_first_name = "";
			for ( int i = 0 ; i < ( command_vector.size() - command_place ) ; i++)
			{
				if ( command_vector[i][0] == '-' )
				{
					temp_first_name.erase(temp_first_name.size());
					new_contact.set_name(temp_first_name);
					i = command_vector.size() - command_place;
					continue;
				}
				else
				{
					temp_first_name = temp_first_name + command_vector[i];
					temp_first_name.push_back(' ');
				}
			}
		}
		else if ( command_vector[command_place] == "-l" )
		{
			string temp_last_name = "";
			for ( int i = 0 ; i < ( command_vector.size() - command_place ) ; i++)
			{
				if ( command_vector[i][0] == '-' )
				{
					temp_last_name.erase(temp_last_name.size());
					new_contact.set_last_name(temp_last_name);
					i = command_vector.size() - command_place;
					continue;
				}
				else
				{
					temp_last_name = temp_last_name + command_vector[i];
					temp_last_name.push_back(' ');
				}
			}
		}
		else if ( command_vector[command_place] == "-a" )
		{
			string temp_address = "";
			for ( int i = 0 ; i < ( command_vector.size() - command_place ) ; i++)
			{
				if ( command_vector[i][0] == '-' )
				{
					temp_address.erase(temp_address.size());
					new_contact.set_address(temp_address);
					i = command_vector.size() - command_place;
					continue;
				}
				else
				{
					temp_address = temp_address + command_vector[i];
					temp_address.push_back(' ');
				}
			}
		}
		else if ( command_vector[command_place] == "-e" )
		{
			string temp_email = "";
			for ( int i = 0 ; i < ( command_vector.size() - command_place ) ; i++)
			{
				if ( command_vector[i][0] == '-' )
				{
					temp_email.erase(temp_email.size());
					if (new_contact.set_email(temp_email))
					{}
					else
					{
						return 0;
					}
					i = command_vector.size() - command_place;
					continue;
				}
				else
				{
					temp_email = temp_email + command_vector[i];
					temp_email.push_back(' ');
				}
			}
		}
		else if ( command_vector[command_place] == "-p" )
		{
			string temp_phone = "";
			for ( int i = 0 ; i < ( command_vector.size() - command_place ) ; i++)
			{
				if ( command_vector[i][0] == '-' )
				{
					temp_phone.erase(temp_phone.size());
					if ( new_contact.set_phone_number(temp_phone) )
					{}
					else
					{
						return 0;
					}
					i = command_vector.size() - command_place;
					continue;
				}
				else
				{
					temp_phone = temp_phone + command_vector[i];
					temp_phone.push_back(' ');
				}
			}
		}
	}
	new_contact.set_id(id);
	return 1;
}

bool test_add(){
	vector<string> v;
	vector<contact> c;
	v[COMMAND_PLACE] = "add";
	v[COMMAND_PLACE+1] = "-f";
	v[COMMAND_PLACE+2] = "SlimShady";
	if ( add(1 , v , c) == 1 )
	{
		return PASS;
	}
	else
	{
		return FAIL;
	}
}

contact search_by_id ( int id_to_be_searched , vector<contact>& contact_vector){
	for ( int i = 0 ; i < contact_vector.size() ; i++ )
	{
		if ( contact_vector[i].id == id_to_be_searched )
		{
			return contact_vector[i];
		}
	}
	return contact_vector[contact_vector.size()];
}

void print_contact ( contact contact_to_be_printed ){
	cout << contact_to_be_printed.id << " " << contact_to_be_printed.name << " " << contact_to_be_printed.last_name << " " << contact_to_be_printed.email << " " << contact_to_be_printed.phone_number << " " << contact_to_be_printed.address << endl;
	return;
}



void search ( string what_to_search , vector<contact>& contact_vector ){
	for ( int i = 0 ; i < contact_vector.size() ; i++ )
	{
		if ( contact_vector[i].search_contact(what_to_search) )
		{
			print_contact ( contact_vector[i] );
		}
	}
	return;
}

void update ( int id , vector<string>& command_vector , vector<contact>& contact_vector ){
	for ( int i = 0 ; i < command_vector.size() ; i++ )
	{
		if ( command_vector[i] == "-f" )
		{
			search_by_id ( id , contact_vector ).set_name(command_vector[i+1]);
		}
		else if ( command_vector[i] == "-l" )
		{
			search_by_id ( id , contact_vector ).set_last_name(command_vector[i+1]);
		}
		else if ( command_vector[i] == "-p" )
		{
			search_by_id ( id , contact_vector ).set_phone_number(command_vector[i+1]);
		}
		else if ( command_vector[i] == "-e" )
		{
			search_by_id ( id , contact_vector ).set_email(command_vector[i+1]);
		}
		else if (command_vector[i] == "-a" )
		{
			string temp_address_string = "";
			temp_address_string = temp_address_string + command_vector[i+1];
			while (command_vector[i+2][0] != '-')
			{
			    temp_address_string = temp_address_string + command_vector[i+2];
				i++;
			}
			search_by_id ( id , contact_vector ).set_address(temp_address_string);
		}
	}
	return;
}

bool test_update(){
	vector<contact> c;
	c[0].set_id(12);
	c[0].set_name("akbar");
	vector<string> v;
	v[0]="update";
	v[1]="12";
	v[2]="-f";
	v[3]="mamad";
	update(12 , v , c);
	if ( c[0].name == "mamad" )
	{
		return PASS;
	}
	else
	{
		return FAIL;
	}
}

int main () {
	int id = 0;
	string command;
	vector<contact> contact_vector;
	while(true){
		cin >> command;
		vector<string> command_vector = get_command(command);
		if ( command_vector[COMMAND_PLACE] == "add" )
		{
			id++;
			if (add ( id , command_vector , contact_vector ) )
			{
				cout << "Command Ok" << endl;
			}
			else
			{
				cout << "Command Failed" << endl;
			}
		}
		if ( command_vector[COMMAND_PLACE] == "search" )
		{
			search ( command_vector[TXT_PLACE] , contact_vector );
		}
		if ( command_vector[COMMAND_PLACE] == "update" )
		{
			update ( stoi( command_vector[ID_PLACE] , 0 , 10 ) , command_vector , contact_vector );
		}
		if ( command_vector[COMMAND_PLACE] == "exit" )
		{
			return 0;
		}
	}
}